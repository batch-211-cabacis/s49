// alert("We are going to simulate an interactive web page using DOM and fetching data from a server");

// let posts = [];

// let count = 1;


/*
	Fetch method
	Syntax:
	fetch('url',options)
	- accepts arguments:
	url -- this is the url which the request is to be made
	options parameter - used only when we need the details of the request from the user

	fetch() method in JS is used to send request in the server and load the received response in the webpage. The request and reponse is in JSON format.
*/

// Get post data

fetch('https://jsonplaceholder.typicode.com/posts')
.then((response)=>response.json())
.then((data)=>showPosts(data));
/*
	use fetch method to get the posts inside https://jsonplaceholder.typicode.com/posts 
	make the response in json format (.then)
*/

// Add post data
document.querySelector('#form-add-post').addEventListener('submit',(e)=>{
	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts',{
		method: 'POST',
		body: JSON.stringify({
			title: document.querySelector('txt-title').value,
			body: document.querySelector('txt-body').vallue,
			userId: 1
		}),
		headers: {'Content-type': 'application/json; charset=UTF-8'}
	})
	.then((response)=>response.json()) //converts the reponse into json format
	.then((data)=>{
		console.log(data);
		alert('Successfully added.')
		document.querySelector('#txt-title').value = null;
		document.querySelector('#txt-body').value = null;
	})

})

// Mini activity

// Show post

const showPosts = (posts) =>{
	let postEntries = "";

	posts.forEach((post)=>{
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onClick="editPost('${post.id}')">Edit</button>
				<button onClick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	})
	console.log(postEntries)
	document.querySelector("#div-post-entries").innerHTML = postEntries
}

// Edit Post

const editPost = (id)=>{
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	// pass the id, title, body from the post to be updated in the edit post form
	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;

	// add attribute
	document.querySelector('#btn-submit-update').removeAttribute('disabled')
};

// Update post

document.querySelector('#form-edit-post').addEventListener('submit',(e)=>{
	e.preventDefault();
	fetch('https://jsonplaceholder.typicode.com/posts/1',{
		method: 'PUT',
		body: JSON.stringify({
			id: document.querySelector('#txt-edit-id').value,
			title: document.querySelector('#txt-edit-title').value,
			body: document.querySelector('#txt-edit-body').value,
			userId: 1
		}),
		headers: {'Content-type': 'application/json; charset=UTF-8'}
	})
	.then((response)=>response.json())
	.then((data)=>{
		console.log(data);
		alert('Successfully updated');

		document.querySelector("#txt-edit-id").value = null;
		document.querySelector("#txt-edit-title").value = null;
		document.querySelector("#txt-edit-body").value = null;

		document.querySelector('#btn-submit-update').setAttribute('disabled',true);

	});
});


// Delete post

// const deletePost = (id) =>{
// 	document.querySelector(`#post-${id}`).remove()
// 	alert('Successfully deleted post')
// }


const deletePost = (id)=>{
	fetch('https://jsonplaceholder.typicode.com/posts/1',{
		method: 'DELETE',
		headers: {'Content-type': 'application/json; charset=UTF-8'}
	})
	.then((response)=>response.json())
	.then((data)=>{
		console.log(data);
		alert('Successfully deleted post');

		document.querySelector(`#post-${id}`).remove()	
	});
};







